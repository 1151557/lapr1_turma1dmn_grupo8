/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.grupo8.mn.laprmaven;

import com.sun.org.apache.bcel.internal.generic.SWAP;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.matrix.DenseMatrix;
import java.io.PrintWriter;


/**
 *
 * @author Newton
 */
public class Caracterizacao {
    
    public static void soma_media(double[][] matriz) {
        double soma_total = 0, media_total = 0;
        double[] linhas = new double[matriz.length - 1];
        double[] colunas = new double[matriz.length - 1];
        double[] media_linhas = new double[matriz.length - 1];
        double[] media_colunas = new double[matriz.length - 1];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                linhas[i] += matriz[i][j];
                colunas[j] += matriz[i][j];
                soma_total += matriz[i][j];
            }
        }
        media_total = soma_total / (matriz.length * matriz.length);

        for (int k = 0; k < matriz.length; k++) {
            media_linhas[k] = linhas[k] / matriz.length;
            media_colunas[k] = colunas[k] / matriz.length;
        }

    }
    public static void eigen(double[][] matriz){
          
          Matrix a = new Basic2DMatrix(matriz);

        EigenDecompositor eigenD = new EigenDecompositor(a);
        Matrix[] mattD = eigenD.decompose();
        
        /*
        for(int i=0; i<matriz.length;i++){
            System.out.println(mattD[i]);
        }
         */
        
      }
    public static void regratrapz(double[][] matriz, int k){
        double[] coluna = new double[k];
        
        for(int i=0; i<k; i++){
            
            double y = Math.pow((matriz[i][k-1]),2) - Math.pow((matriz[i][0]),2);
            int h = k-1;
            double resultado = Math.round((h*y)/4);
            coluna[i] = resultado;
        }
        /*for(int j=0; j<k; j++){
            System.out.println("Na posição "+(j+1)+" o valor da matriz coluna é = "+coluna[j));
        }
        */
    }
}
