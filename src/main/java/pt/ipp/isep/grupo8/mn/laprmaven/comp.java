/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.grupo8.mn.laprmaven;

/**
 *
 * @author Asus
 */
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
import org.la4j.Matrix;

import org.la4j.matrix.DenseMatrix;

import org.la4j.matrix.dense.Basic2DMatrix;

import org.la4j.decomposition.EigenDecompositor;
import org.la4j.decomposition.SingularValueDecompositor;
import org.la4j.matrix.dense.Basic1DMatrix;

public class comp {

    public static void compressao(double matriz[][], String nomeficheiro) throws IOException {
//double matComparacao[][]={{0.8, 0.3},{0.2,0.7}};
        // Criar objeto do tipo Matriz
        int valores = 0;
        do {
            System.out.println("Introduza o número de valores singulares a serem utilizados na reconstrução da imagem");
            Scanner scanner = new Scanner(System.in);
            try {
                valores = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Introduza um número!");
            }
            
        } while (valores <= 0 || valores > matriz.length);

        Matrix a = new Basic2DMatrix(matriz);

        // SVD decomposition
        SingularValueDecompositor eigenD = new SingularValueDecompositor(a);

        Matrix[] mattD = eigenD.decompose();

        /*for (int i = 0; i < 3; i++) {

            System.out.println(mattD[i]);

        }
         */
        double[] uss = {0.00, 0.00, 0.71, 0.71, 0.00, 0.00};
        double[] vs = {0.00, 0.10, 0.21, 0.26, 0.66, 0.66};
        double ds = 544.17;

        /*  Matrix us = new Basic1DMatrix(1, 6, uss);
        Matrix vsss = new Basic1DMatrix(1, 6, vs).transpose();
        // Matrix dss = new Basic2DMatrix(ds);
        /*for (double s : usss) {
            System.out.println(s);
        }
         */
 /*
        Matrix as = us.multiply(dss);
        Matrix asdsa = as.multiply(vsss);
        
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                
            }
        }
        
        Matrix ud = mattD[0].multiply(dss);
        Matrix trans = mattD[2].transpose();
        Matrix fin = ud.multiply(trans);
        
        System.out.println(fin);
         */
        // converte objeto Matrix (duas matrizes)  para array Java
        double matU[][] = mattD[0].toDenseMatrix().toArray();

        double matD[][] = mattD[1].toDenseMatrix().toArray();

        double matV[][] = mattD[2].toDenseMatrix().toArray();

        //gravar_mostrar.grava_compr(matU, matD, matV, nomeficheiro + "_comprimido");
        descompressaoS(uss, ds, vs,valores);
    }

    public static double[][] descompressaoS(double matU[], double matD, double matV[], int valor) {
        double matriznova[][] = new double[matU.length][matU.length];
        for (int i = 0; i < matU.length; i++) {
            for (int j = 0; j < matU.length; j++) {
                /*  System.out.println(matD[i][i]);
                System.out.println(matU[i][j]);
                System.out.println(matV[i][j]);
                 */
                matriznova[i][j] += matU[i] * matV[j] * matD;

            }
        }
        for (int j = 0; j < matriznova.length; j++) {
            for (int l = 0; l < matriznova[j].length; l++) {
                System.out.print(matriznova[j][l] + " ,");
            }
            System.out.println();
        }
        return matriznova;
    }

    public static void descompressaoM(double matU[][], double matD[][], double matV[][], int valor) {
        double matriznova[][] = new double[matU.length][matU.length];
        for (int i = 0; i < matU.length; i++) {
            for (int j = 0; j < matU.length; j++) {
                /*  System.out.println(matD[i][i]);
                System.out.println(matU[i][j]);
                System.out.println(matV[i][j]);
                 */
                for (int k = 0; k < matD.length; k++) {
                    matriznova[i][j] += matU[i][k] * matD[k][k] * matV[j][k];
                }
            }
        }
        /*for (int j = 0; j < matriznova.length; j++) {
            for (int l = 0; l < matriznova[j].length; l++) {
                System.out.print(matriznova[j][l] + " ,");
            }
            System.out.println();
        } 
         */
    }
}
