/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.grupo8.mn.laprmaven;

import com.sun.org.apache.bcel.internal.generic.SWAP;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.matrix.DenseMatrix;
import java.io.PrintWriter;



/**
 * @author Newton
 **/

public class Filtros {
    public static double[][] filtrodemedia(double[][] matriz) {
        double matrizmedia[][] = new double[matriz.length + 2][matriz.length + 2];
        for (int i = 1; i < matriz.length + 1; i++) {
            for (int j = 1; j < matriz.length + 1; j++) {
                matrizmedia[i][j] = matriz[i - 1][j - 1];
            }

        }
        double matriznova[][] = new double[matriz.length][matriz.length];

        for (int i = 0; i < matrizmedia.length - 2; i++) {
            for (int j = 0; j < matrizmedia.length - 2; j++) {
                matriznova[i][j] = (matrizmedia[i][j] + matrizmedia[i][j + 1] + matrizmedia[i][j + 2] + matrizmedia[i + 1][j] + matrizmedia[i + 2][j] + matrizmedia[i + 2][j + 1] + matrizmedia[i + 2][j + 2] + matrizmedia[i + 1][j + 2] + matrizmedia[i + 1][j + 1]) / 9;
            }
        }
        /*
        for (int j = 0; j < matriznova.length; j++) {
            for (int l = 0; l < matriznova[j].length; l++) {
                System.out.print(matriznova[j][l] + " ,");
            }
            System.out.println();
        }
         */
        return matriz;
    }

    public static double[][] filtrodemediana(double[][] matriz) {
        double matrizmediana[][] = new double[matriz.length + 2][matriz.length + 2];
        for (int i = 1; i < matriz.length + 1; i++) {
            for (int j = 1; j < matriz.length + 1; j++) {
                matrizmediana[i][j] = matriz[i - 1][j - 1];
            }

        }
        double matriznova[][] = new double[matriz.length][matriz.length];
        for (int i = 0; i < matrizmediana.length - 2; i++) {
            for (int j = 0; j < matrizmediana.length - 2; j++) {
                double arrayzona[] = {matrizmediana[i][j], matrizmediana[i][j + 1], matrizmediana[i][j + 2], matrizmediana[i + 1][j], matrizmediana[i + 2][j], matrizmediana[i + 2][j + 1], matrizmediana[i + 2][j + 2], matrizmediana[i + 1][j + 2], matrizmediana[i + 1][j + 1]};
                Arrays.sort(arrayzona);
                matriznova[i][j] = arrayzona[4];
            }
        }
        /*
        for (int j = 0; j < matriznova.length; j++) {
            for (int l = 0; l < matriznova[j].length; l++) {
                System.out.print(matriznova[j][l] + " ,");
            }
            System.out.println();
        }
         */
        return matriznova;

    }

    public static double[][] filtrodemaximo(double[][] matriz) {
        double matrizmaximo[][] = new double[matriz.length + 2][matriz.length + 2];
        for (int i = 1; i < matriz.length + 1; i++) {
            for (int j = 1; j < matriz.length + 1; j++) {
                matrizmaximo[i][j] = matriz[i - 1][j - 1];
            }

        }
        double matriznova[][] = new double[matriz.length][matriz.length];
        for (int i = 0; i < matrizmaximo.length - 2; i++) {
            for (int j = 0; j < matrizmaximo.length - 2; j++) {
                double arrayzona[] = {matrizmaximo[i][j], matrizmaximo[i][j + 1], matrizmaximo[i][j + 2], matrizmaximo[i + 1][j], matrizmaximo[i + 2][j], matrizmaximo[i + 2][j + 1], matrizmaximo[i + 2][j + 2], matrizmaximo[i + 1][j + 2], matrizmaximo[i + 1][j + 1]};
                Arrays.sort(arrayzona);
                matriznova[i][j] = arrayzona[8];
            }
        }
        /*
        for (int j = 0; j < matriznova.length; j++) {
            for (int l = 0; l < matriznova[j].length; l++) {
                System.out.print(matriznova[j][l] + " ,");
            }
            System.out.println();
        }
         */
        return matriznova;
    }

    public static double[][] filtrodeminimo(double[][] matriz) {
        double matrizminimo[][] = new double[matriz.length + 2][matriz.length + 2];
        for (int i = 1; i < matriz.length + 1; i++) {
            for (int j = 1; j < matriz.length + 1; j++) {
                matrizminimo[i][j] = matriz[i - 1][j - 1];
            }

        }
        double matriznova[][] = new double[matriz.length][matriz.length];
        for (int i = 0; i < matrizminimo.length - 2; i++) {
            for (int j = 0; j < matrizminimo.length - 2; j++) {
                double arrayzona[] = {matrizminimo[i][j], matrizminimo[i][j + 1], matrizminimo[i][j + 2], matrizminimo[i + 1][j], matrizminimo[i + 2][j], matrizminimo[i + 2][j + 1], matrizminimo[i + 2][j + 2], matrizminimo[i + 1][j + 2], matrizminimo[i + 1][j + 1]};
                Arrays.sort(arrayzona);
                matriznova[i][j] = arrayzona[0];
            }
        }
        /*
        for (int j = 0; j < matriznova.length; j++) {
            for (int l = 0; l < matriznova[j].length; l++) {
                System.out.print(matriznova[j][l] + " ,");
            }
            System.out.println();
        }*/
        return matriznova;
    }
    
    public static double[][] filtroconvulcao(double[][] matriz, double[][] mascara) {
        double matrizconv[][] = new double[matriz.length + 2][matriz.length + 2];
        matrizconv[0][0] = matriz[0][0];
        matrizconv[matrizconv.length - 1][matrizconv.length - 1] = matriz[matriz.length - 1][matriz.length - 1];
        matrizconv[0][matrizconv.length - 1] = matriz[0][matriz.length - 1];
        matrizconv[matrizconv.length - 1][0] = matriz[matriz.length - 1][0];

        for (int i = 1; i < matriz.length + 1; i++) {
            for (int j = 1; j < matriz.length + 1; j++) {
                matrizconv[i][j] = matriz[i - 1][j - 1];
            }
        }
        for (int i = 1; i < matrizconv.length - 1; i++) {
            matrizconv[0][i] = matriz[0][i - 1];
            matrizconv[i][0] = matriz[i - 1][0];
            matrizconv[matrizconv.length - 1][i] = matriz[matriz.length - 1][i - 1];
            matrizconv[i][matrizconv.length - 1] = matriz[i - 1][matriz.length - 1];
        }
        double matriznova[][] = new double[matriz.length][matriz.length];
        for (int i = 0; i < matriznova.length; i++) {
            for (int j = 0; j < matriznova.length; j++) {
                matriznova[i][j] = (matrizconv[i][j] * mascara[0][0] + matrizconv[i][j + 1] * mascara[0][1] + matrizconv[i][j + 2] * mascara[0][2]
                        + matrizconv[i + 1][j] * mascara[1][0] + matrizconv[i + 1][j + 1] * mascara[1][1] + matrizconv[i + 1][j + 2] * mascara[1][2]
                        + matrizconv[i + 2][j] * mascara[2][0] + matrizconv[i + 2][j + 1] * mascara[2][1] + matrizconv[i + 2][j + 2] * mascara[2][2]);
            }
        }
        /* for (int j = 0; j < matriznova.length; j++) {
            for (int l = 0; l < matriznova[j].length; l++) {
                System.out.print(matriznova[j][l] + " ");
            }
            System.out.println();
        }*/
        return matriz;
    }

    public static double[][] mascara() {

        double mascara[][] = new double[3][3];
        System.out.println("Introduza os valores da matriz máscara 3x3");
        System.out.println("");
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("Introduza o número da posição " + (i + 1) + " " + (j + 1) + ": ");
                mascara[i][j] = scan.nextInt();
            }
        }

        /*
        for (int i = 0; i < matriz.length; i++)
        {
            for (int j = 0; j < matriz.length; j++)
            {
                System.out.print(matriz[i][j]+"\t");
            }
             
            System.out.println();
        }
         */
        return mascara;
    }
    
    public static double[][] filtrodevar(double[][] matriz) {

        double matrizvar[][] = new double[matriz.length + 2][matriz.length + 2];
        matrizvar[0][0] = matriz[0][0];
        matrizvar[matrizvar.length - 1][matrizvar.length - 1] = matriz[matriz.length - 1][matriz.length - 1];
        matrizvar[0][matrizvar.length - 1] = matriz[0][matriz.length - 1];
        matrizvar[matrizvar.length - 1][0] = matriz[matriz.length - 1][0];

        for (int i = 1; i < matriz.length + 1; i++) {
            for (int j = 1; j < matriz.length + 1; j++) {
                matrizvar[i][j] = matriz[i - 1][j - 1];
            }
        }
        for (int i = 1; i < matrizvar.length - 1; i++) {
            matrizvar[0][i] = matriz[0][i - 1];
            matrizvar[i][0] = matriz[i - 1][0];
            matrizvar[matrizvar.length - 1][i] = matriz[matriz.length - 1][i - 1];
            matrizvar[i][matrizvar.length - 1] = matriz[i - 1][matriz.length - 1];
        }

        double matriznova[][] = new double[matriz.length][matriz.length];
        for (int i = 0; i < matriznova.length; i++) {
            for (int j = 0; j < matriznova.length; j++) {
                double soma = (matrizvar[i][j] + matrizvar[i][j + 1] + matrizvar[i][j + 2]
                        + matrizvar[i + 1][j] + matrizvar[i + 1][j + 1] + matrizvar[i + 1][j + 2]
                        + matrizvar[i + 2][j] + matrizvar[i + 2][j + 1] + matrizvar[i + 2][j + 2]);
                soma = (soma * soma) / 9;
                double somaquadrado = (matrizvar[i][j] * matrizvar[i][j] + matrizvar[i][j + 1] * matrizvar[i][j + 1] + matrizvar[i][j + 2] * matrizvar[i][j + 2]
                        + matrizvar[i + 1][j] * matrizvar[i + 1][j] + matrizvar[i + 1][j + 1] * matrizvar[i + 1][j + 1] + matrizvar[i + 1][j + 2] * matrizvar[i + 1][j + 2]
                        + matrizvar[i + 2][j] * matrizvar[i + 2][j] + matrizvar[i + 2][j + 1] * matrizvar[i + 2][j + 1] + matrizvar[i + 2][j + 2] * matrizvar[i + 2][j + 2]);
                matriznova[i][j] = (somaquadrado - soma) / 8;
            }
        }
        /*for (int j = 0; j < matriznova.length; j++) {
            for (int l = 0; l < matriznova[j].length; l++) {
                System.out.print(matriznova[j][l] + " ");
            }
            System.out.println();
        }*/

        return matriz;
    }
}
