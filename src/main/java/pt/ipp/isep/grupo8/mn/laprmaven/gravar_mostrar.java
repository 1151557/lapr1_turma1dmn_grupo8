/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.grupo8.mn.laprmaven;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Math.round;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author Newton
 */
public class gravar_mostrar {

    public static double[][] mostrarMatriz(double[][] matriz) {
        for (int j = 0; j < matriz.length; j++) {
            for (int l = 0; l < matriz[j].length; l++) {
                System.out.print(matriz[j][l] + " ");
            }
            System.out.println();
        }
        return matriz;
    }

    public static void gravar(double[][] matriz, String nomeficheiro) throws IOException {
        PrintWriter gravarArq = new PrintWriter(nomeficheiro + ".txt");
        for (int i = 0; i < matriz.length; i++) {
            gravarArq.println();
            gravarArq.print(matriz[i][0]);
            for (int j = 1; j < matriz[i].length; j++) {
                gravarArq.printf("," + matriz[i][j]);
            }
        }
        gravarArq.close();
        System.out.printf("Ficheiro guardado na pasta de execução do programa");
    }

    public static void grava_compr(double[][] matU, double[][] matD, double[][] matV, String nomeficheiro) throws IOException {
        PrintWriter gravarArq = new PrintWriter(nomeficheiro + ".txt");
        DecimalFormat df = new DecimalFormat("0.00");
        DecimalFormatSymbols sym = DecimalFormatSymbols.getInstance();
        sym.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(sym);
        gravarArq.println(ler_file.primeiralinha);
        for (int i = 0; i < matU.length; i++) {
            gravarArq.println();
            gravarArq.println(df.format(matD[i][i]));
            gravarArq.print(df.format(matU[0][i]));
            for (int j = 1; j < matU.length; j++) {
                gravarArq.print("," + df.format(matU[j][i]));
            }
            gravarArq.println();
            gravarArq.print(df.format(matV[0][i]));
            for (int k = 1; k < matU.length; k++) {
                gravarArq.print("," + df.format(matV[k][i]));
            }
        }
        gravarArq.close();
        System.out.printf("Ficheiro guardado na pasta de execução do programa");

    }
}
