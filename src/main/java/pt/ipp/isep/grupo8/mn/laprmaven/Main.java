/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.grupo8.mn.laprmaven;

import com.sun.org.apache.bcel.internal.generic.SWAP;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.matrix.DenseMatrix;
import java.io.PrintWriter;
import pt.ipp.isep.grupo8.mn.laprmaven.ler_file;
import pt.ipp.isep.grupo8.mn.laprmaven.gravar_mostrar;
import pt.ipp.isep.grupo8.mn.laprmaven.Filtros;
import pt.ipp.isep.grupo8.mn.laprmaven.carct;
import pt.ipp.isep.grupo8.mn.laprmaven.comp;
import pt.ipp.isep.grupo8.mn.laprmaven.sort_modes;
import pt.ipp.isep.grupo8.mn.laprmaven.Gnuplot;

/**
 *
 * @author Newton
 */
public class Main {

    public static void main(String[] args) throws IOException {

        System.out.println("***********************************************************");
        System.out.println("****************Processamento de Imagem********************");
        System.out.println("***********************************************************");
        if (args.length > 0 && args.length < 2) {
            String ficheiroinput = args[0];
            double matrizoriginal[][] = null;
            double matrizcopia[][] = null;
            double mascaracopia[][] = null;
            try {
                matrizoriginal = ler_file.lerficheiro(ficheiroinput);
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Ficheiro input não encontrado");
                System.exit(0);
            }

            if (matrizoriginal != null) {
                matrizcopia = matrizoriginal;
                String nomeficheiro = ficheiroinput;
                //String nome[] = nomeficheiro.split(".");
                //System.out.println(nome[1]);
                String escolha;
                Scanner scanner = new Scanner(System.in);
                do {
                    menu();
                    escolha = scanner.next();
                    switch (escolha) {
                        case "1":
                            System.out.println("Caracterização da matriz foi escolhido");
                            carct.menu_caract(matrizcopia);
                            break;
                        case "2":
                            System.out.println("Filtro de convolução foi escolhido");
                            mascaracopia = Filtros.mascara();
                            matrizcopia = Filtros.filtroconvulcao(matrizcopia, mascaracopia);
                            nomeficheiro = nomeficheiro + "_convolucao";
                            break;

                        case "3":
                            System.out.println("Filtro de média foi escolhido");
                            matrizcopia = Filtros.filtrodemedia(matrizcopia);
                            nomeficheiro = nomeficheiro + "_media";
                            break;

                        case "4":
                            System.out.println("Filtro de variância foi escolhido");
                            matrizcopia = Filtros.filtrodevar(matrizcopia);
                            nomeficheiro = nomeficheiro + "_variancia";
                            break;

                        case "5":
                            System.out.println("Filtro de mediana foi escolhido");
                            matrizcopia = Filtros.filtrodemediana(matrizcopia);
                            nomeficheiro = nomeficheiro + "_mediana";
                            break;

                        case "6":
                            System.out.println("Filtro de máximo foi escolhido");
                            matrizcopia = Filtros.filtrodemaximo(matrizcopia);
                            nomeficheiro = nomeficheiro + "_max";
                            break;

                        case "7":
                            System.out.println("Filtro de mínimo foi escolhido");
                            matrizcopia = Filtros.filtrodeminimo(matrizcopia);
                            nomeficheiro = nomeficheiro + "_min";
                            break;

                        case "8":
                            System.out.println("Rotação de matriz 90 graus");
                            matrizcopia = Rotacao.rotacao(matrizcopia);
                            nomeficheiro = nomeficheiro + "_rot90";
                            break;

                        case "9":
                            System.out.println("Rotação de matriz -90 graus");
                            matrizcopia = Rotacao.rotacaom90(matrizcopia);
                            nomeficheiro = nomeficheiro + "_rot.menos.90";
                            break;

                        case "10":
                            System.out.println("Compressão de imagem foi escolhido");
                            nomeficheiro = nomeficheiro + "_compressao";
                            comp.compressao(matrizcopia, nomeficheiro);
                            break;

                        case "11":
                            System.out.println("Mostrar Matriz");
                            gravar_mostrar.mostrarMatriz(matrizcopia);
                            break;

                        case "12":
                            System.out.println("Guardar matriz foi selecionado");
                            gravar_mostrar.gravar(matrizcopia, nomeficheiro);
                            break;

                        case "13":
                            System.out.println("Selection Sort");
                            sort_modes.seletionsort(matrizcopia);
                            break;

                        case "14":
                            System.out.println("Vizualizar imagem");
                            //                         Gnuplot.plot(matrizcopia);
                            break;

                        case "0":
                            System.out.println("Programa terminado com sucesso");
                            break;

                        default:
                            System.out.println("Opção inválida");
                    }
                } while (!escolha.equals("0"));
            } else if (args.length == 0) {
                System.out.println("Introduza a localização da imagem/matriz!");
            } else if (args.length > 1) {
                System.out.println("Apenas um parâmetro deve ser introduzido");
            } else if (matrizoriginal == null) {
                System.out.println("Matriz inválida!");
            }
        }
    }

    private static void menu() {
        System.out.println("\n");
        System.out.println("**********************************************");
        System.out.println("                    MENU");
        System.out.println("**********************************************");
        System.out.println("Escolha uma das opções");
        System.out.println("1 - Caracterização da matriz");
        System.out.println("2 - Filtro de convulção");
        System.out.println("3 - Filtro de média");
        System.out.println("4 - Filtro de variância");
        System.out.println("5 - Filtro de mediana");
        System.out.println("6 - Filtro de máximo");
        System.out.println("7 - Filtro de mínimo");
        System.out.println("8 - Rotação 90 graus");
        System.out.println("9 - Rotação -90 graus");
        System.out.println("10 - Compressão de imagem");
        System.out.println("11 - Mostrar Matriz");
        System.out.println("12 - Guardar Matriz");
        System.out.println("13 - Ordenar matriz");
        System.out.println("14 - Vizualizar imagem");
        System.out.println("0 - Sair");
        System.out.println("Introduza uma opção:");

    }
}
