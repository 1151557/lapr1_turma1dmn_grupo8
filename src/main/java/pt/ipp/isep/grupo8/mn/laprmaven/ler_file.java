/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.grupo8.mn.laprmaven;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import pt.ipp.isep.grupo8.mn.laprmaven.Main;

/**
 *
 * @author Newton
 */
public class ler_file {

    public static String primeiralinha = null;

    public static double[][] lerficheiro(String ficheiroinput) throws IOException {
        try {
            File file = new File(ficheiroinput);
            int linhas = 0;
            FileReader in = new FileReader(file);
            BufferedReader br = new BufferedReader(in);
            while (br.readLine() != null) {
                linhas++; //contar o numero de linhas para criar a array com o numero certo
            }
            String semi[] = new String[linhas];
            int i = 0;
            Scanner input = new Scanner(file);
            while (input.hasNext()) {
                semi[i++] = input.nextLine();
            }
            input.close();
            //JOptionPane.showMessageDialog(null, semi);  

            String matriz[][] = new String[linhas - 2][linhas - 2];
            int k = 0;
            primeiralinha = semi[0];
            for (int j = 2; j < semi.length; j++) {
                //JOptionPane.showMessageDialog(null, semi[j]); 
                matriz[k] = semi[j].split(",");
                k++;
            }
            for (int j = 0; j < linhas - 2; j++) {
                for (int l = 0; l < linhas - 2; l++) {
                    matriz[j][l] = matriz[j][l].trim();
                }
            }

            double matrizdouble[][] = new double[linhas - 2][linhas - 2];
            for (int j = 0; j < linhas - 2; j++) {
                for (int l = 0; l < linhas - 2; l++) {
                    matrizdouble[j][l] = Double.valueOf(matriz[j][l]);
                }
            }
            /*
             * for (int j = 0; j < linhas - 2; j++) { for (int l = 0; l < linhas
             * - 2; l++) { System.out.println(matrizdouble[j][l]); } }
             *
             */
            in.close();
            br.close();
            return matrizdouble;
        } catch (Exception e) {
            try {
                File file = new File(ficheiroinput);
                int linhas = 0;
                FileReader in = new FileReader(file);
                BufferedReader br = new BufferedReader(in);
                while (br.readLine() != null) {
                    linhas++; //contar o numero de linhas para criar a array com o numero certo
                }
                String semi[] = new String[linhas];
                int i = 0;
                Scanner input = new Scanner(file);
                while (input.hasNext()) {
                    semi[i++] = input.nextLine();
                }
                input.close();
                int valoressing = (linhas - 2) / 3;
                //System.out.println(valoressing);
                if (valoressing == 1) {
                    String matUS[] = semi[3].split(",");
                    String matVS[] = semi[4].split(",");
                    for (int j = 0; j < linhas - 2; j++) {
                        matUS[j] = matUS[j].trim();
                        matVS[j] = matVS[j].trim();
                    }
                    double D = Double.valueOf(semi[2].trim());
                    double matU[] = new double[matUS.length];
                    double matV[] = new double[matUS.length];

                    for (int j = 0; j < matUS.length; j++) {
                        matU[j] = Double.valueOf(matUS[j]);
                        matV[j] = Double.valueOf(matVS[j]);
                    }

                    return comp.descompressaoS(matU, D, matV, valoressing);

                } else {

                    String D[] = new String[valoressing];
                    int l = 0;
                    for (int j = 2; j < semi.length;) {
                        D[l] = (semi[j]);
                        if (j + 3 < semi.length) {
                            j = j + 3;
                        } else {
                            j = semi.length;
                        }
                        l++;
                    }
                    double d[] = new double[D.length];
                    for (int j = 0; j < D.length; j++) {
                        d[j] = Double.valueOf(D[j]);
                    }

                    String matrizU[][] = new String[semi[3].split(",").length][valoressing];
                    String matrizV[][] = new String[semi[3].split(",").length][valoressing];

                    for (int j = 3; j < semi.length;) {
                        String U[] = semi[j].split(",");
                        String V[] = semi[j + 1].split(",");
                        /* for (int m = 0; m < U.length; m++) {
                        System.out.println(U[m]);
                    }
                         */
                        for (int k = 0; k < semi[3].split(",").length; k++) {
                            for (int m = 0; m < valoressing; m++) {
                                matrizU[k][m] = U[m];
                            }

                        }
                        if (j + 3 < semi.length) {
                            j = j + 3;
                        } else {
                            j = semi.length;
                        }
                    }

                    /* for (int j = 0; j < matrizU.length; j++) {
                        for (int m = 0; l < matrizU[j].length; l++) {
                            System.out.print(matrizU[j][m] + " ,");
                        }
                        System.out.println();
                    }
                     */
                }
            } catch (Exception f) {
                System.out.println("Matriz mal formada!");
            }

        }
        return null;
    }
}
