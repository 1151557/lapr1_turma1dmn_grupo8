/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.grupo8.mn.laprmaven;

import com.sun.org.apache.bcel.internal.generic.SWAP;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.matrix.DenseMatrix;
import java.io.PrintWriter;


/**
 *
 * @author Newton
 */
public class carct {
    
    public static void menu_caract(double[][] matriz){
        String escolha;
                Scanner scanner = new Scanner(System.in);
        
        do{
            menu();
            escolha = scanner.next();
            switch (escolha) {
                case"1": 
                    soma_media(matriz);
                    break;
                case"2": 
                    eigen(matriz);
                    break;
                case"3": 
                    regratrapz(matriz, matriz.length);
                    break;
                case "0":
                    System.out.println("Programa terminado com sucesso");
                    break;
                default:
                    System.out.println("Opção inválida"); 
            }
        }while (!escolha.equals("0"));
    }
    
    public static void soma_media(double[][] matriz) {
        double soma_total = 0, media_total = 0;
        double[] linhas = new double[matriz.length];
        double[] colunas = new double[matriz.length];
        double[] media_linhas = new double[matriz.length];
        double[] media_colunas = new double[matriz.length];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                linhas[i] += matriz[i][j];
                colunas[j] += matriz[i][j];
                soma_total += matriz[i][j];
            }
        }
        media_total = soma_total / (matriz.length * matriz.length);

        for (int k = 0; k < matriz.length; k++) {
            media_linhas[k] = linhas[k] / matriz.length;
            media_colunas[k] = colunas[k] / matriz.length;
        }
        
        System.out.println("*******Soma e média******");
        System.out.println("");
        for(int i=0; i<matriz.length; i++){
            System.out.println("Soma da linha"+i+" = "+ linhas[i]);
            System.out.println("A média da linha"+i+" = "+ media_linhas[i]);
        }
        System.out.println("");
        for(int i=0; i<matriz.length; i++){
            System.out.println("Soma da coluna"+i+" = "+ colunas[i]);
            System.out.println("A média da coluna"+i+" = "+ media_colunas[i]);
        }
        System.out.println("");
        System.out.println("A Soma de todos os elementos da matriz é = "+soma_total);
        System.out.println("A media da matriz é ="+media_total);
        
    }
    public static void eigen(double[][] matriz){
          
          Matrix a = new Basic2DMatrix(matriz);

        EigenDecompositor eigenD = new EigenDecompositor(a);
        Matrix[] mattD = eigenD.decompose();
        
        System.out.println("*******Eigen value Decomposition******");
        System.out.println("");
        for(int i=0; i<2;i++){
            System.out.println("eigen value ["+i+"] = "+mattD[i]);
        }
      }
    public static void regratrapz(double[][] matriz, int k){
        double[] coluna = new double[k];
        
        for(int i=0; i<k; i++){
            
            double y = Math.pow((matriz[i][k-1]),2) - Math.pow((matriz[i][0]),2);
            int h = k-1;
            double resultado = Math.round((h*y)/4);
            coluna[i] = resultado;
        }
        System.out.println("*******Regra do trapézio******");
        for(int j=0; j<k; j++){
            System.out.println("Na posição "+(j+1)+" o valor da matriz coluna é = "+coluna[j]);
        }
    }
    public static void menu(){
        System.out.println("**********CARACTERIZAÇÃO************");
        System.out.println("Escolha uma das opções");
        System.out.println("1 - Soma e média de cada uma das linhas, colunas e de todos os elementos da matriz");
        System.out.println("2 - Eigein Value Decomposition");
        System.out.println("3 - Regra do trapézio");
        System.out.println("0 - Sair");
        System.out.println("Introduza uma opção:");
    }
}
