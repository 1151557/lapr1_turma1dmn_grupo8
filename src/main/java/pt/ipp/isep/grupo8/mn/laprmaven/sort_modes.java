/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.grupo8.mn.laprmaven;

/**
 *
 * @author Newton
 */
public class sort_modes {
    
    private static void insertionsort(double[][] matrizcopia) {
        double matrizinsert[][] = new double[matrizcopia.length][matrizcopia.length];
        double array[] = new double[matrizcopia.length - 1];
        int q;
        for (int i = 0; i < matrizcopia.length; i++) {
            for (int p = 1; p < matrizcopia.length; p++) {
                double temp = matrizcopia[p][i];
                for (q = p - 1; q >= 0 && temp > matrizcopia[q][i]; q--) {
                    matrizcopia[q + 1][i] = matrizcopia[q][i];
                }
                matrizcopia[q + 1][i] = temp;
            }
        }
        /*
        for (int j = 0; j < matrizcopia.length; j++) {
            for (int l = 0; l < matrizcopia[j].length; l++) {
                System.out.print(matrizcopia[j][l] + " ");
            }
            System.out.println();
        }
         */
    }

    private static void bubblesort(double[][] matrizcopia) {
        for (int k = 0; k < matrizcopia.length; k++) {
            for (int j = 0; j < matrizcopia.length; j++) {
                for (int i = 0; i < matrizcopia.length - 1; i++) {
                    if (matrizcopia[i][k] < matrizcopia[i + 1][k]) {
                        double temp = matrizcopia[i][k];
                        matrizcopia[i][k] = matrizcopia[i + 1][k];
                        matrizcopia[i + 1][k] = temp;
                    }
                }
            }
        }
        /*
        for (int j = 0; j < matrizcopia.length; j++) {
            for (int l = 0; l < matrizcopia[j].length; l++) {
                System.out.print(matrizcopia[j][l] + " ");
            }
            System.out.println();
        }
         */
    }

    public static void seletionsort(double[][] matrizcopia) {
        for (int i = 0; i < matrizcopia.length; i++) {//coluna
            for (int j = 0; j < matrizcopia.length; j++) {
                int min_indx = j;
                for (int k = j + 1; k < matrizcopia.length; k++) {
                    if (matrizcopia[k][i] > matrizcopia[min_indx][i]) {
                        min_indx = k;
                    }
                }
                double temp = matrizcopia[min_indx][i];
                matrizcopia[min_indx][i] = matrizcopia[j][i];
                matrizcopia[j][i] = temp;
            }
        }
        /*
        for (int j = 0; j < matrizcopia.length; j++) {
            for (int l = 0; l < matrizcopia[j].length; l++) {
                System.out.print(matrizcopia[j][l] + " ");
            }
            System.out.println();
        }
         */
    }
}
