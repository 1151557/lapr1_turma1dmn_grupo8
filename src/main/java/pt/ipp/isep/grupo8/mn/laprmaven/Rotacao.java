/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.grupo8.mn.laprmaven;

import com.sun.org.apache.bcel.internal.generic.SWAP;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.matrix.DenseMatrix;
import java.io.PrintWriter;

/**
 *
 * @author Newton
 */
public class Rotacao {
   
    public static double[][] rotacao(double[][] matriz) {
        double matrizrotacao[][] = new double[matriz.length][matriz.length];
        for (int j = 0; j < matrizrotacao.length; j++) {
            for (int l = 0; l < matrizrotacao.length; l++) {
                matrizrotacao[j][l] = matriz[matriz.length - l - 1][j];
            }
        }
        /*
        for (int j = 0; j < matrizrotacao.length; j++) {
            for (int l = 0; l < matrizrotacao[j].length; l++) {
                System.out.print(matrizrotacao[j][l] + " ");
            }
            System.out.println();
        }
         */
        return matrizrotacao;
    }
    
    public static double[][] rotacaom90(double[][] matriz) {
        double matrizrotacao[][] = new double[matriz.length][matriz.length];
        for (int j = 0; j < matrizrotacao.length; j++) {
            for (int l = 0; l < matrizrotacao.length; l++) {
                matrizrotacao[j][l] = matriz[l][matriz.length - j - 1];
            }
        }
        /*
        for (int j = 0; j < matrizrotacao.length; j++) {
            for (int l = 0; l < matrizrotacao[j].length; l++) {
                System.out.print(matrizrotacao[j][l] + " ");
            }
            System.out.println();
        }
         */
        return matrizrotacao;
    }
}
